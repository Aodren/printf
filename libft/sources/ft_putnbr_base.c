/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/04 19:46:13 by abary             #+#    #+#             */
/*   Updated: 2016/01/31 19:03:44 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_putnbrp_base(unsigned long nbr, unsigned long base)
{
	if (nbr >= base)
		ft_putnbrp_base(nbr / base, base);
	if (nbr % base < 10)
		ft_putchar('0' + nbr % base);
	else
		ft_putchar('A' + nbr % base - 10);
}

void		ft_putnbr_base(unsigned long nbr, unsigned long base)
{
	ft_putnbrp_base(nbr, base);
}
