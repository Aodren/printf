/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 13:27:08 by abary             #+#    #+#             */
/*   Updated: 2016/02/04 15:07:36 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libftprintf.h"
#include "libft.h"
#include <stdarg.h>
#include <wchar.h>
#include <locale.h>
#include <stdarg.h>
#include <limits.h>

/*
** unsigned int pour flag digit et X gerer ???
*/

int		main(void)
{
	//setlocale(LC_ALL, "");
	double test = 2500;
//	ft_printf("%ll#x", 9223372036854775807);
	//ft_printf("%zhd", "4294967296");
	ft_printf("%a\n", test);
	printf("%a", test);
//	ft_printf("%x", 42);
	return (0);
}
