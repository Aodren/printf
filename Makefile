# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: abary <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/23 12:31:57 by abary             #+#    #+#              #
#    Updated: 2016/01/19 19:35:20 by abary            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

lib_DIR = libft

NAME = printf

#CFLAGS = -Wall -Werror -Wextra

SRC = main.c

OBJ = (SRC:.c=.o)

CC = gcc

$(NAME) :
	(cd $(lib_DIR) && $(MAKE))
	$(CC) -o ft_printf $(SRC) libft/libftprintf.a -I libft/includes

all : $(NAME) 

run : printf

clean :
	rm -rf $(OBJ)

fclean : clean
	rm -rf $(NAME)

re : fclean all

.PHONY: all clean flcean re
